# OpenML dataset: road-safety

https://www.openml.org/d/44038

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset used in the tabular data benchmark https://github.com/LeoGrin/tabular-benchmark,  
                          transformed in the same way. This dataset belongs to the "classification on categorical and
                          numerical features" benchmark. Original description: 
 
Data reported to the police about the circumstances of personal injury road accidents in Great Britain from 1979, and the maker and model information of vehicles involved in the respective accident.

This version includes data up to 2015.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44038) of an [OpenML dataset](https://www.openml.org/d/44038). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44038/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44038/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44038/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

